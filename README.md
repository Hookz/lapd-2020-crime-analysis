# LAPD 2020 Crime Analysis

This project is a data analysis of crime reports from Los Angeles Police Department's districts. The data covers the timeframe from January 1, 2020 until March 15, 2021. 


## Team
* Wafaa Aljbawi
* Janneke van Baden
* Michal Jarski 

## Contents
* [data.csv](data.csv) - file containing crime reports from Los Angeles Police Department's \[LAPD\] districts between January 1, 2020 and March 15, 2021
* [districts.geojson](districts.geojson) - shapes of LAPD police districts for map creation
* [precincts.geojson](precincts.geojson) - shapes of LAPD police precincts for map creation

## Data Sources
* [Crime Data from 2020 to Present (Retrieved on March 15, 2021)](https://data.lacity.org/Public-Safety/Crime-Data-from-2020-to-Present/2nrs-mtv8)
* [LAPD Reporting Districts (GeoJSON data, retrieved on March 19, 2021)](https://geohub.lacity.org/datasets/lapd-reporting-districts?geometry=-118.979%2C33.621%2C-117.842%2C34.418)

Additionally, LAPD Precinct GeoJSON data was obtained using the Reporting Districts data, with the help of [Mapshaper](https://mapshaper.org) to dissolve the data into precincts. 

## Results
Results can be seen in the [Crime Analysis notebook](crime_analysis.ipynb). 

If no map is displayed it means that the notebook was converted to a static file by Gitlab. To see all file contents please refer to the [static html file](crime_analysis.html) or run the notebook locally and trust the file.

To execute the notebook locally, clone this repository and run:

```pip install -r requirements.txt```.

Then the notebook can be opened with ```jupyter notebook``` where it can be run from scratch.

### Video
A short video was produced as part of this project. The result is presented below:

![LAPD Infinity War: Will it ever be over? A Crime Analysis (2020-2021)](videos/video.mp4)

Link to YouTube video in case the inline video isn't displayed: [https://www.youtube.com/watch?v=5keGXDE6sbU](https://www.youtube.com/watch?v=5keGXDE6sbU)
